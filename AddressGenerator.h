//
// Created by joelr on 09.06.2022.
//

#ifndef AUFGABE3_ADDRESSGENERATOR_H
#define AUFGABE3_ADDRESSGENERATOR_H

#include "setup.h"
#include <string>
#include <utility>
/**
 * Base class for and virtual address generator.
 *
 * Those are used to create virtual addresses with different algorithms to e.g. test the page replacing efficiency
 */
class AddressGenerator {
public:

    const std::string NAME; //!< The name of the address generator. Must be set by any inheriting class

    explicit AddressGenerator(std::string name) : NAME(std::move(name)) {};
    virtual ~AddressGenerator() = default;

    /**
     * @brief Resets the address generator. Does nothing by default. Should be called if the current page table is replaced (e.g process change)
     *
     * Can be overwritten if generator requires specific resets after a page table changes
     */
    virtual inline void reset() {

    }

    /**
     * @brief Generate the address
     *
     * @return The generated address
     */
    virtual int gen_address() = 0;
};


#endif //AUFGABE3_ADDRESSGENERATOR_H
