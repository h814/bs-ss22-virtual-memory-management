//
// Created by joelr on 09.06.2022.
//

#include "BubbleAddressGenerator.h"
#include <cmath>

BubbleAddressGenerator::BubbleAddressGenerator() : AddressGenerator("Bubble") {}

int BubbleAddressGenerator::gen_address() {

    if (page_cnt_intenal >= PAGES_PROCESS_CNT - 1) {
        page_cnt_intenal = 0;
        offset_cnt_internal = 0;
    }

    if (offset_cnt_internal % PAGE_SIZE == 0 && offset_cnt_internal != 0) {
        ++page_cnt_intenal;
    }

    if(offset_cnt_internal >= pow(2, OFFSET_BITS)) {
        offset_cnt_internal = 0;
    }

    int page_num = page_cnt_intenal;
    int offset = offset_cnt_internal;

    int address;
    address = page_num << 5;
    address |= offset;


    ++offset_cnt_internal;
    return address;
}

/**
 * Resets the internal page counter and internal offset counter
 */
void BubbleAddressGenerator::reset() {
    page_cnt_intenal = 0;
    offset_cnt_internal = 0;
}
