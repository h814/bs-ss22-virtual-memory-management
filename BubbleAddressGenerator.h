//
// Created by joelr on 09.06.2022.
//

#ifndef AUFGABE3_BUBBLEADDRESSGENERATOR_H
#define AUFGABE3_BUBBLEADDRESSGENERATOR_H

#include "AddressGenerator.h"

/**
 * @brief Generates virtual addresses bubbleing up from 0 to the max value
 *
 * Goes through all possible virtual addresses from page 0 to #PAGES_PROCESS_CNT. The offset goes from 0 to 2^{#OFFSET_BITS} in each case.
 *
 * So if there are 8 pages per process (0 to 7 with 3 bit) and 5 offset bits (32) then this generator will go from 0 to 255
 */
class BubbleAddressGenerator : public AddressGenerator {
private:
    inline static int page_cnt_intenal = 0; //!< The internal counter to determine the page for the address generation
    inline static int offset_cnt_internal = 0; //!< The internal counter to determine the current offset for the address generation
public:

    BubbleAddressGenerator();
    void reset() override;
    int gen_address() override;
};
#endif //AUFGABE3_BUBBLEADDRESSGENERATOR_H
