//
// Created by joelr on 09.06.2022.
//

#include "CPU.h"

#include <random>
#include <algorithm>
#include <iostream>

#include "BubbleAddressGenerator.h"
#include "OS.h"

CPU::CPU(OS *os) : os(os), mmu(MMU([ObjectPtr = os](auto &&PH1, auto &&PH2) {
    return ObjectPtr->page_failure_interrupt(std::forward<decltype(PH1)>(PH1), std::forward<decltype(PH2)>(PH2));
}, &data_integrity_map)) {
    auto getChar = []() {
        static const std::string genData = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!§$%&/()=?-_.,";
        static int cnt = 0;

        if(cnt >= genData.size() - 1) {
            cnt = 0;
        }

        ++cnt;

        return genData.at(cnt);
    };


    //save random chars to disk
    for (int i = 0; i < PROCESS_COUNT; ++i) {
        os->page_tables.at(i) = new Pagetable;
        os->processes.at(i) = new Process();

        for(int j=0;j<PAGES_PROCESS_CNT;++j) {
            for(int k=0;k<PAGE_SIZE / 8;++k) {
                auto page_offset = (j*(PAGE_SIZE/8))+k;
                auto full_offset = (i*PAGE_SIZE/8 * PAGES_PROCESS_CNT) + page_offset;
                auto c = getChar();
                os->disk.at(full_offset) = static_cast<std::byte>(c);
            }
        }
    }

}

/**
 * @brief Set up the cpu and prepare for sim start.
 *
 * Sets the active process as well as the active page table. Resets the address gen
 */
void CPU::init() {
    addressGenerator->reset();
    os->active_table = os->page_tables.at(0);
    os->replacer->set_table(os->active_table);
    active_process = os->processes.at(0);
}

void CPU::cycle() {
    std::random_device rd;
    std::uniform_int_distribution<int> distribution(1, 100);
    std::mt19937 engine(rd());

    int value = distribution(engine);

    if (value <= CHANGE_PROCESS_WS) {
        this->change_process();
    } else if (value <= CHANGE_PROCESS_WS + WRITE_WS) {
        this->write_data();
    } else {
        this->read_data();
    }
}

void CPU::change_process() {
    auto set_process_and_table = [this](const int &index) {
        this->active_process = os->processes.at(index);
        os->active_table = os->page_tables.at(index);
        os->replacer->set_table(os->active_table);
    };

    for(int i=0;i<os->processes.size();++i) {
        if(os->processes.at(i) == this->active_process) {
            int index = 0;
            if(i+1 < os->processes.size()) {
                index = i+1;
            }
            set_process_and_table(index);
            return;
        }
    }

    set_process_and_table(0);
}

void CPU::read_data() {
    auto address_load = addressGenerator->gen_address();
    os->setReferenced(address_load >> OFFSET_BITS);
    address_load = mmu.translate_address(active_process->pid(), os->active_table, address_load, PAGE_SIZE);

    if(!assert_data_integrety(address_load)) {
        throw std::runtime_error("Segmentation fault (sim)");
    }

    int address_index = address_load >> OFFSET_BITS;
    int address_bit_offset = address_load % 8;

    auto mask = static_cast<std::byte>(1 << address_bit_offset);
    auto data = os->ram.at(address_index);
    auto bit = data & mask;
    int i=(int)bit;
}

void CPU::write_data() {
    auto address_store = addressGenerator->gen_address();

    os->setReferenced(address_store >> OFFSET_BITS);
    os->setModified(address_store >> OFFSET_BITS);

    address_store = mmu.translate_address(active_process->pid(), os->active_table, address_store, PAGE_SIZE);

    if(!assert_data_integrety(address_store)) {
        throw std::runtime_error("Segmentation fault (sim)");
    }
}

/**
 * Checks if a data access to a specific address should be allowed or not
 *
 * @param address The address of the data to check
 * @return true if the data access is allowed, false otherwise
 */
bool CPU::assert_data_integrety(int address) {
    int address_index = address >> OFFSET_BITS;

    return std::ranges::any_of(this->data_integrity_map, [address_index, active_pid = active_process->pid()](auto tuple){
        auto & [frame_start, frame_end, process_pid] = tuple;
        return address_index >= frame_start && address_index <= frame_end && process_pid == active_pid;
    });
}

/**
 * @brief Set a specific AddressGenerator.
 *
 * Must be called before the sim start, because it defaults to nullptr
 *
 * @param addressGenerator The address generator to set
 */
void CPU::set_address_generator(AddressGenerator *addressGenerator) {
    this->addressGenerator = addressGenerator;
}
