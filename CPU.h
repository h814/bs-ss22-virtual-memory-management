//
// Created by joelr on 09.06.2022.
//

#ifndef AUFGABE3_CPU_H
#define AUFGABE3_CPU_H

#include <stdexcept>

#include "setup.h"
#include "MMU.h"
#include "AddressGenerator.h"
#include "Process.h"



class OS;

/**
 * @brief Simulates the CPU of the system
 *
 * Holds the MMU and AddressGenerator of the simulator. Is responsible for assuring data integrity of read and write and for the process changes. It cycles with a given randomness.
 * The probability for a write is CPU#WRITE_WS and for a process change CPU#CHANGE_PROCESS_WS
 */
class CPU {

    friend class OS;

private:

    //static constexpr int READ_WS = 50;
    static constexpr int WRITE_WS = 30;
    static constexpr int CHANGE_PROCESS_WS = 20;

    OS *os;
    MMU mmu;
    AddressGenerator* addressGenerator = nullptr;

    Process *active_process;

    void change_process();
    void read_data();
    void write_data();

    integritiy_map data_integrity_map;

    bool assert_data_integrety(int address);
public:

    explicit CPU(OS *os);
    void set_address_generator(AddressGenerator* addressGenerator);

    void cycle();
    void init();
};

#endif //AUFGABE3_CPU_H
