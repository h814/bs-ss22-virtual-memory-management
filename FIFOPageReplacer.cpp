//
// Created by joelr on 14.06.2022.
//

#include "FIFOPageReplacer.h"

#include <iostream>

FIFOPageReplacer::FIFOPageReplacer(RAM *ram, Disk *disk) : PageReplacer(ram, disk, "FIFO Replacer") {}

int FIFOPageReplacer::run_replacer(int process_pid, int page_number) {
    std::array<std::byte, PAGE_SIZE/8> data{};

    //Push new page to back of queue
    auto newPage = this->active_pagetable->at(page_number);
    this->pages.push(newPage);

    auto is_inserted = insert_into_free_ram_frame(process_pid, page_number, data);
    if(is_inserted != -1) {
        return is_inserted;
    }

    if(this->pages.empty()) throw std::runtime_error("No free ram frame found but also nothing to replace");

    // Remove with fifo principle
    PagetableEntry *page = this->pages.front();
    this->pages.pop();

    if(page == nullptr) {
        throw std::runtime_error("no page to remove found");
    }

    auto frame = page->Frame;

    if(page->modified) {
        write_back_modified_data(frame, process_pid, page_number);
    }

    // Reset removed pagetable entry
    page->modified = false;
    page->referenced = false;
    page->present = false;
    page->Frame = -1;

    std::copy(data.begin(), data.end(), ram->begin() + frame );
    return frame;
}
