//
// Created by joelr on 14.06.2022.
//

#ifndef AUFGABE3_FIFOPAGEREPLACER_H
#define AUFGABE3_FIFOPAGEREPLACER_H

#include <queue>

#include "PageReplacer.h"

class FIFOPageReplacer : public PageReplacer{
private:
    std::queue<PagetableEntry*> pages;
public:

    explicit FIFOPageReplacer(RAM *ram, Disk *disk);

    int run_replacer(int process_pid, int page_number) override;

};


#endif //AUFGABE3_FIFOPAGEREPLACER_H
