//
// Created by joelr on 09.06.2022.
//

#include "MMU.h"

MMU::MMU(std::function<int(int process_pid, int page_number)> interrupt_call, integritiy_map *data_integritiy_map) : interrupt_call(std::move(interrupt_call)), data_integritiy_map(data_integritiy_map) {}

/**
 * @brief Translates a virtual address to a physical address.
 *
 * @param process_pid The active process id
 * @param active_table The active page table
 * @param address The address to translate
 * @param page_size The size of one page
 * @return The translated physical address
 */
int MMU::translate_address(int process_pid, Pagetable* active_table, int address, const int page_size) const {
    int page_index = address / page_size;
    auto address_offset = address % page_size;

    auto frame = active_table->at(page_index)->Frame;

    if(frame < 0) {
        auto newFrame = interrupt_call(process_pid, page_index);
        active_table->at(page_index)->Frame = newFrame;
        active_table->at(page_index)->present = true;
        frame = newFrame;

        data_integritiy_map->at(frame / (PAGE_SIZE / 8)) = std::make_tuple(newFrame, newFrame + PAGE_SIZE / 8 - 1, process_pid);
    }

    int real_address;
    real_address = frame << OFFSET_BITS;
    real_address |= address_offset;

    return real_address;
}