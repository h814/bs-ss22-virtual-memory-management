//
// Created by joelr on 09.06.2022.
//

#ifndef AUFGABE3_MMU_H
#define AUFGABE3_MMU_H

#include <functional>

#include "setup.h"

class MMU {
private:
    std::function<int(int process_pid, int page_number)> interrupt_call;
    integritiy_map *data_integritiy_map;
public:

    explicit MMU(std::function<int(int process_pid, int page_number)> interrupt_call, integritiy_map *data_integritiy_map);

    int translate_address(int process_pid, Pagetable* active_table, int address, int page_size) const;
};

#endif //AUFGABE3_MMU_H
