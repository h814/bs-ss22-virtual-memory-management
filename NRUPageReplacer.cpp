//
// Created by joelr on 09.06.2022.
//

#include "NRUPageReplacer.h"
NRUPageReplacer::NRUPageReplacer(RAM *ram, Disk *disk) : PageReplacer(ram, disk, "NRU Replacer") {}

int NRUPageReplacer::run_replacer(int process_pid, int page_number) {

    std::array<std::byte, PAGE_SIZE/8> data{};

    auto is_inserted = insert_into_free_ram_frame(process_pid, page_number, data);
    if(is_inserted != -1) {
        return is_inserted;
    }

    // Find page with smallest class
    PagetableEntry *page = nullptr;
    int smallest_class = 4;
    for(auto & i : *active_pagetable) {

        if(!i->present) continue;

        auto page_class = get_class(i);
        if(page_class < smallest_class) {
            page = i;
            smallest_class = page_class;
        }
    }

    smallest_class = 4;
    bool hasFound = false;
    for(auto pagetable : this->page_tables) {
        for(auto & i : *pagetable) {

            if(!i->present) continue;

            auto page_class = get_class(i);
            if(page_class < smallest_class) {
                page = i;
                smallest_class = page_class;

                if(page_class == 0) {
                    hasFound = true;
                    break;
                }
            }
        }

        if(hasFound) break;
    }

    if(page == nullptr) {
        throw std::runtime_error("no page to remove found");
    }

    auto frame = page->Frame;


    if(page->modified) {
        write_back_modified_data(frame, process_pid, page_number);
    }

    // Reset removed pagetable entry
    page->modified = false;
    page->referenced = false;
    page->present = false;
    page->Frame = -1;


    std::copy(data.begin(), data.end(), ram->begin() + frame );
    return frame;
}

int NRUPageReplacer::get_class(const PagetableEntry * const pagetableEntry) {
    if(!pagetableEntry->referenced && !pagetableEntry->modified) {
        return 0;
    }else if(!pagetableEntry->referenced && pagetableEntry->modified) {
        return 1;
    }else if(pagetableEntry->referenced && !pagetableEntry->modified) {
        return 2;
    }else {
        return 3;
    }
}
