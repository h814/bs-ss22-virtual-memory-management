//
// Created by joelr on 09.06.2022.
//

#ifndef AUFGABE3_NRUPAGEREPLACER_H
#define AUFGABE3_NRUPAGEREPLACER_H

#include "PageReplacer.h"

class NRUPageReplacer : public PageReplacer{
private:
    static int get_class(const PagetableEntry * pagetableEntry) ;
public:

    explicit NRUPageReplacer(RAM *ram, Disk *disk);

    int run_replacer(int process_pid, int page_number) override;

};


#endif //AUFGABE3_NRUPAGEREPLACER_H
