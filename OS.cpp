//
// Created by joelr on 09.06.2022.
//

#include "OS.h"

#include "NRUPageReplacer.h"
#include <iostream>
#include <iomanip>

OS::OS() :  replacer(nullptr), cpu(CPU(this)) {
    for(const auto pagetable : this->page_tables) {
        for(int i=0;i<pagetable->size();++i) {
            pagetable->at(i) = new PagetableEntry();
        }
    }
}

void OS::startSim(int cycle_cnt){

    if(this->replacer == nullptr) throw std::runtime_error("No replacer set");
    if(cpu.addressGenerator == nullptr) throw std::runtime_error("No address generator set");

    cpu.init();

    this->cycles = cycle_cnt;
    auto i = 0;

    printInfo();

    while (i < this->cycles) {
        cpu.cycle();
        ++i;
    }

    printResult();
}

int OS::page_failure_interrupt(int process_pid, int page_number) {
    ++this->page_failures;
    auto ret = replacer->run_replacer(process_pid, page_number);
    clear_ref_bits();
    return ret;
}

void OS::setReferenced(int page_index, bool value) {
    this->active_table->at(page_index)->referenced = value;
}

void OS::setModified(int page_index, bool value) {
    this->active_table->at(page_index)->modified = value;
}

void OS::clear_ref_bits() {
    for(auto &entry : *this->active_table) {
        entry->referenced = false;
    }
}

void OS::set_address_generator(AddressGenerator *addressGenerator) {
    this->cpu.set_address_generator(addressGenerator);
}

void OS::printInfo() const {
    using std::cout, std::endl;

    cout << "\tIMPORTANT NOTICES" << endl;
    cout << "-----------------------------------" << endl;
    cout <<"The RAM and the disk are arrays of the type byte so the size is RAM_SIZE / 8" << endl;
    cout << endl;
    cout << "IMPORTANT for working is that if calculations are made it needs to be checked\n"
         <<"if the used Size must be divided by 8 (e.g RAM_SIZE, PAGE_SIZE often meet this requirement)" << endl;

    cout << endl << "\tGeneral info" << endl;
    cout << "-----------------------------------" << endl;
    cout << "Starting Simulation with " << cycles << " CPU cycles" << endl;
    cout << "Current address generator:\t\t" << cpu.addressGenerator->NAME << endl;
    cout << "Current page replacing algorithm:\t" << replacer->NAME << endl;

    cout << endl << "\tArchitecture Info" << endl;
    cout << "-----------------------------------" << endl;
    cout << "Process Count:\t\t\t" << PROCESS_COUNT << endl;
    cout << "Page Size:\t\t\t" << PAGE_SIZE <<endl;
    cout << "Address Size:\t\t\t" << ADDRESS_SIZE << " ( Offset Bits: " << OFFSET_BITS << "; Address Bits: "<< PAGE_INDEX_BITS << " )" << endl;
    cout << "RAM Size:\t\t\t" << RAM_SIZE << endl;
    cout << "Page count in RAM:\t\t" << PAGES_RAM_CNT << endl;
    cout << "Address Room:\t\t\t" << ADDRESS_ROOM << endl;
    cout << "Pages per process count:\t" << PAGES_PROCESS_CNT << endl;
}

void OS::printResult() const {
    using std::cout, std::endl;

    double page_fault_rate = this->page_failures / (double)this->cycles * 100;

    cout << endl;
    cout << "-----------------------------------" << endl;
    cout << endl;
    cout << "Simulator finished successfully" << endl;
    cout << endl << "\t     Result" << endl;
    cout << "-----------------------------------" << endl;
    cout << "Page fault count: " << this->page_failures << endl;
    cout << "Page fault rate: " << std::setprecision(3) << page_fault_rate << "%" << endl;
}
