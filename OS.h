//
// Created by joelr on 09.06.2022.
//

#ifndef AUFGABE3_OS_H
#define AUFGABE3_OS_H

#include "setup.h"
#include "Process.h"
#include "PageReplacer.h"
#include "CPU.h"

class CPU;

template<class T>
concept IsPageReplacer = std::is_base_of<PageReplacer, T>::value;

class OS {
private:
    friend class CPU;

    RAM ram{};
    Disk disk{};
    std::array<Process *, PROCESS_COUNT> processes{};
    std::array<Pagetable *, PROCESS_COUNT> page_tables{};
    Pagetable *active_table = nullptr;

    PageReplacer* replacer;
    CPU cpu;

    void clear_ref_bits();

    /**
     * Sim fields
     * */

    int page_failures = 0;
    int cycles;

    void printInfo() const;
    void printResult() const;
public:
    explicit OS();

    void startSim(int cycle_cnt);

    [[nodiscard]] int page_failure_interrupt(int process_pid, int page_number);

    void setReferenced(int page_index, bool value = true);
    void setModified(int page_index, bool value = true);

    void set_address_generator(AddressGenerator* addressGenerator);

    template<IsPageReplacer T>
    inline void set_replacer() {
        this->replacer = new T(&this->ram, &this->disk);
        this->replacer->set_tables(this->page_tables);
    }
};

#endif //AUFGABE3_OS_H
