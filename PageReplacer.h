//
// Created by joelr on 09.06.2022.
//

#ifndef AUFGABE3_PAGEREPLACER_H
#define AUFGABE3_PAGEREPLACER_H

#include "setup.h"
#include "ram.h"

#include <string>
#include <stdexcept>

/**
 * Page replacer is the base for every page replacing algorithm
 */
class PageReplacer {
protected:
    RAM *ram;
    Disk *disk;
    Pagetable *active_pagetable;
    std::array<Pagetable*, PROCESS_COUNT> page_tables{};

    /**
     * @brief Calculates and loads the data of a page from the disk.
     *
     * Requires the process pid and page number to calculate the position on the disk.
     *
     * @param process_pid The process which owns the data
     * @param page_number The page on which the data can be found
     * @return The read data
     */
    std::array<std::byte, PAGE_SIZE/8> load_page_from_disk(int process_pid, int page_number) {

        auto page_offset = (page_number*(PAGE_SIZE/8));
        auto full_offset = (process_pid*PAGE_SIZE/8 * PAGES_PROCESS_CNT) + page_offset;

        std::array<std::byte, PAGE_SIZE/8> data{};
        std::copy(this->disk->begin() + full_offset, this->disk->begin() +full_offset +  PAGE_SIZE/8, data.begin());

        return data;
    }

    /**
     * @brief Writes the data of the RAM back to the DISK.
     *
     * @param frame The frame in the RAM where the data is placed
     * @param process_pid The process which owns the data
     * @param page_number The page on which the data is stored
     */
    void write_back_modified_data(int frame, int process_pid, int page_number) {
        auto frame_start = frame * (PAGE_SIZE / 8);
        auto frame_end = frame_start + (PAGE_SIZE / 8);
        std::array<std::byte, PAGE_SIZE/8> wb_data{};
        std::copy(ram->begin() + frame_start, ram->begin() + frame_end, wb_data.begin());
        write_back_data_to_disk(process_pid, page_number, wb_data);
    }

    /**
     * @brief Saves given data of a Process to disk
     *
     * @param process_pid The process which owns the data
     * @param page_number The page on which the data is stored
     * @param data The data to save
     */
    void write_back_data_to_disk(int process_pid, int page_number, std::array<std::byte, PAGE_SIZE/8> &data) {
        auto page_offset = (page_number*(PAGE_SIZE/8));
        auto full_offset = (process_pid*PAGE_SIZE/8 * PAGES_PROCESS_CNT) + page_offset;

        std::copy(data.begin(), data.end(),  this->disk->begin() + full_offset);
    }

    /**
     * @brief Tries to find a free frame in RAM. If found save the data there.
     *
     * @param process_pid The process which owns the data
     * @param page_number The page on which the data is stored
     * @param data The data to save in the RAM
     * @return The free frame or -1 if no frame was found
     */
    int insert_into_free_ram_frame(int process_pid, int page_number, std::array<std::byte, PAGE_SIZE/8> &data) {
        if(active_pagetable == nullptr) throw std::runtime_error("No pagetable set");

        data = load_page_from_disk(process_pid, page_number); // 32 Bit Seite

        //If free frame in ram put there
        int free_frame = RAMUtil::get_next_free_frame(ram);
        if(free_frame != -1) {
            std::copy(data.begin(), data.end() ,ram->begin() + free_frame );
        }

        return free_frame;
    }

public:

    const std::string NAME;

    explicit PageReplacer(RAM *ram, Disk *disk, std::string name) : ram(ram), disk(disk), active_pagetable(nullptr), NAME(std::move(name)) {}

    void set_table(Pagetable *active_table) {
        this->active_pagetable = active_table;
    }

    void set_tables(std::array<Pagetable*, PROCESS_COUNT>  tables) {
        this->page_tables = tables;
    }

    virtual int run_replacer(int process_pid, int page_number) = 0;
};

#endif //AUFGABE3_PAGEREPLACER_H
