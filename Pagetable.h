//
// Created by joelr on 09.06.2022.
//

#ifndef AUFGABE3_PAGETABLE_H
#define AUFGABE3_PAGETABLE_H

/**
 * Data structure of a single entry in a page table
 */
struct PagetableEntry {
    int Frame = -1; //!< In which physical frame the page is placed, if not present -1 (default)
    bool modified = false; //<! Was the entry written to (requires write back)
    bool referenced = false; //<! Was entry referenced read or written
    bool present = false; //<! if is currently present should be false if Frame is -1 otherwise true
};


#endif //AUFGABE3_PAGETABLE_H
