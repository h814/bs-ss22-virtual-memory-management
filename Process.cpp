//
// Created by joelr on 09.06.2022.
//

#include "Process.h"

int Process::pid_cnt = 0; //!< pid counter starts with 0

Process::Process() : _pid(pid_cnt++) {}

int Process::pid() const { return _pid; };