//
// Created by joelr on 09.06.2022.
//

#ifndef AUFGABE3_PROCESS_H
#define AUFGABE3_PROCESS_H

/**
 * Simple process structure. Doesn't do anything special.
 * Just holds a counter for the pid and an individual pid for each process.
 */
class Process {
private:
    static int pid_cnt;

    int _pid;
public:
    Process();

    [[nodiscard]] int pid() const;
};


#endif //AUFGABE3_PROCESS_H
