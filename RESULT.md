# Result

## Adressgeneratoren

### Bubble

Geht alle mögliche Virtuellen-Adressen von Seite 0 bis MAX_PAGE für alle 0 bis MAX_PROCESSES durch. Der Offset geht dabei jeweils von 0 bis $`2^{OFFSET_BITS}`$ 

### Random Walk

Erzeugt eine zufällige Adresse auf Basis der letzten Adresse. Der Startwert ist die mitte aller möglichen Adressen. Der zufällige Wert liegt im Bereich 4 bis 43 und ist mit 50/50 positiv oder negativ.

## Seitenersetzungsalgorithmen

### FIFO (First In First Out)

Wird mit einer queue realisiert. Somit wird die Seite, welche schon am längsten im RAM ist ausgelagert.

#### Beurteilung FIFO
 - Intensiv genutzte Seiten werden genauso behandelt, wie wenig benutzte.
 - Also ist die Qualität der Entscheidung kaum besser als einer Zufallsauswahl.
 - Unverändert wird FIFO in der Praxis sehr selten eingesetzt.

### NRU (Not Recently Used)

Ausgewählt zum Auslagern wird eine Seite, aus der kleinsten nicht leeren Klasse aus 4 möglichen Kategorien die abhängig sind von den Modifiziert- und Referenziertbits. Bei einem Seitenfehler werden alle Referenziertbits zurückgesetzt.

#### Beurteilung NRU

 - Leicht zu verstehen
 - Effiziente Implementierung möglich.
 - Die Leistung könnte besser sein, ist aber oft ausreichend.

## Hypothese

Es wird mit 700 Zyklen simuliert. Die restliche Konfiguration war wie folgt:

- Process Count:                  2
- Page Size:                      32
- Address Size:                   8 (Offset Bits: 5; Address Bits: 3)
- RAM Size:                       128
- Page count in RAM:              4
- Address Room:                   256
- Pages per process count:        8

Die Erwartung ist, dass der Bubble Adressen Generator weniger Seitenfehler erzeugen wird, als die RandomWalk Variante (in FIFO & NRU konstellation).
Aufgrund der Beschreibung und Funktion der beiden Seitenersetzungsalgorithmen vermute ich das der NRU Algorithmus besser performt.


## Messung

| **Adressgenerator** | **Seitenersetzung** | **Anzahl Zugriffe** | **Anzahl Seitenfehler** | **Seitenfehlerrate** |
|---------------------|---------------------|---------------------|-------------------------|----------------------|
| Bubble              | FIFO                | 700                 | 38                      | 5.43%                |
| Bubble              | NRU                 | 700                 | 36                      | 5.14%                |
| Random Walk         | FIFO                | 700                 | 172                     | 24.6%                |
| Random Walk         | NRU                 | 700                 | 200                     | 28.6%                |


### Interpretation

Es zu sehen, dass der Bubble Algorithmus eine sehr geringe Seitenfehlerrate hat, das ist darauf zurückzuführen, dass die Adresse sich immer um eins erhöht und nacheinander eine Seite abgearbeitet wird. Der Random Walk hat eine höhere Fehlerrate.
Beim Bubble-Adressierungs verfahren ist die performance von FIFO & NRU ähnlich. Es gibt lediglich einen Unterschied von 0,29 %. Auch beim Radom Walk verfahren sind die Unterschiede recht gering. Allerdings geht hier der FIFO Algorithmus als klarer Sieger heraus (Differenz von 4 %). 
