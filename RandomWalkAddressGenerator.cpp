//
// Created by joelr on 14.06.2022.
//

#include "RandomWalkAddressGenerator.h"

RandomWalkAddressGenerator::RandomWalkAddressGenerator() : AddressGenerator("RandomWalk") {}

/**
 * @brief Creates an address based on the last one with a little bit of randomness
 *
 * If no last address is present it starts with max / 2.
 * The random part is with a probability of 50% positive and with 50% negative.
 * The random value range is between delta_min and delta_max.
 * Delta gets generated in gen_delta().
 *
 * @return The generated address
 */
int RandomWalkAddressGenerator::gen_address() {
    static int last_address = max / 2;

    last_address += get_delta();

    if(last_address >= max) last_address = max;
    if(last_address <= min) last_address = min;

    return last_address;
}

/**
 * Generates a random value between delta_min and delta_max.
 *
 * Can be positive or negative with a probability of 50% positive and with 50% negative
 *
 * @return The delta value
 */
int RandomWalkAddressGenerator::get_delta() {
    std::random_device rd;
    std::uniform_int_distribution<int> distribution_delta(delta_min, delta_max);
    std::uniform_int_distribution<int> distribution_sign(0, 100);
    std::mt19937 engine(rd());

    int delta = distribution_delta(engine);
    int sign = distribution_sign(engine);

    if(sign >= 50) {
        delta = 0 - delta;
    }

    return delta;
}
