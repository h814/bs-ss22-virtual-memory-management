//
// Created by joelr on 14.06.2022.
//

#ifndef AUFGABE3_RANDOMWALKADDRESSGENERATOR_H
#define AUFGABE3_RANDOMWALKADDRESSGENERATOR_H

#include "AddressGenerator.h"

#include <random>

class RandomWalkAddressGenerator : public AddressGenerator{
private:
    static constexpr int min = 0;
    static constexpr int max = ADDRESS_ROOM - 1;
    static constexpr int delta_min = 4;
    static constexpr int delta_max = 43;

    static int get_delta();
public:

    RandomWalkAddressGenerator();

    int gen_address() override;
};


#endif //AUFGABE3_RANDOMWALKADDRESSGENERATOR_H
