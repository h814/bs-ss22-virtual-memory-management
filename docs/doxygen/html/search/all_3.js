var searchData=
[
  ['change_5fprocess_15',['change_process',['../classCPU.html#a77ff17578dd4cc45add0661132875882',1,'CPU']]],
  ['change_5fprocess_5fws_16',['CHANGE_PROCESS_WS',['../classCPU.html#a976074fb0c889d2c35242932d2789f2e',1,'CPU']]],
  ['clear_5fref_5fbits_17',['clear_ref_bits',['../classOS.html#af6c26f56eebd112c6844dfa0e1ef86cf',1,'OS']]],
  ['cpu_18',['CPU',['../classCPU.html',1,'']]],
  ['cpu_19',['cpu',['../classOS.html#a00faa63dd26cda0c3b9a8a0f5ea39231',1,'OS']]],
  ['cpu_20',['CPU',['../classOS.html#a421a70ac7bb7c7b4eed0fc44b76802d3',1,'OS::CPU()'],['../classCPU.html#a750d06d72c02a95a3ade1eaf56cc2dbf',1,'CPU::CPU()']]],
  ['cpu_2ecpp_21',['CPU.cpp',['../CPU_8cpp.html',1,'']]],
  ['cpu_2eh_22',['CPU.h',['../CPU_8h.html',1,'']]],
  ['cycle_23',['cycle',['../classCPU.html#a3f16142dd2d69d412cd3799945d13bb7',1,'CPU']]],
  ['cycles_24',['cycles',['../classOS.html#a6332de3b744cfb56e60fa38977ed64dc',1,'OS']]]
];
