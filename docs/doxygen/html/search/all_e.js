var searchData=
[
  ['set_5faddress_5fgenerator_105',['set_address_generator',['../classCPU.html#ab5f371a497e252e1b4f5a28c3e922209',1,'CPU::set_address_generator()'],['../classOS.html#a66c2f89d3f87752e4ebfe4216d52db2b',1,'OS::set_address_generator(AddressGenerator *addressGenerator)']]],
  ['set_5freplacer_106',['set_replacer',['../classOS.html#a662ef7a1a311eccb4b871631891f6c9e',1,'OS']]],
  ['set_5ftable_107',['set_table',['../classPageReplacer.html#a284bc194a3e94a0f0eabf576d6fcac61',1,'PageReplacer']]],
  ['set_5ftables_108',['set_tables',['../classPageReplacer.html#abdc420b131a343348944d308e2d5b198',1,'PageReplacer']]],
  ['setmodified_109',['setModified',['../classOS.html#acf918e3352bcf90f83923e89f1299a97',1,'OS']]],
  ['setreferenced_110',['setReferenced',['../classOS.html#ac20e08fef493f529f70c89a4502bebad',1,'OS']]],
  ['setup_2eh_111',['setup.h',['../setup_8h.html',1,'']]],
  ['startsim_112',['startSim',['../classOS.html#ae8583e0ae806b0efe405a5a5d611cdb5',1,'OS']]]
];
