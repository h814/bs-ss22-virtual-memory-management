var indexSectionsWithContent =
{
  0: "_abcdfgilmnoprstw~",
  1: "abcfmnopr",
  2: "r",
  3: "abcfmnoprs",
  4: "abcfgilmnoprstw~",
  5: "_acdfimnoprw",
  6: "dipr",
  7: "co",
  8: "aopr",
  9: "ar"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "related",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Friends",
  8: "Macros",
  9: "Pages"
};

