var searchData=
[
  ['page_5fcnt_5fintenal_220',['page_cnt_intenal',['../classBubbleAddressGenerator.html#a4b09e17c60ba3bf51f235a296641b8e3',1,'BubbleAddressGenerator']]],
  ['page_5ffailures_221',['page_failures',['../classOS.html#a086cd8f29506fd17ed92468cfc1c4261',1,'OS']]],
  ['page_5findex_5fbits_222',['PAGE_INDEX_BITS',['../setup_8h.html#ab8604a8f13b16a74879669e300442c76',1,'setup.h']]],
  ['page_5ftables_223',['page_tables',['../classOS.html#aa970f075ae34eb509376ba44b6dfe1ad',1,'OS::page_tables()'],['../classPageReplacer.html#a2d59c211a74b1ffe6e9960af091d4688',1,'PageReplacer::page_tables()']]],
  ['pages_224',['pages',['../classFIFOPageReplacer.html#a03a8b39a2450eb12cbe328555380aa59',1,'FIFOPageReplacer']]],
  ['pages_5fprocess_5fcnt_225',['PAGES_PROCESS_CNT',['../setup_8h.html#a636d5a26a7f4ba8dcb96c59229387b0e',1,'setup.h']]],
  ['pages_5fram_5fcnt_226',['PAGES_RAM_CNT',['../setup_8h.html#af120b2c3d009bd9f83e1ecc9b211f20b',1,'setup.h']]],
  ['pid_5fcnt_227',['pid_cnt',['../classProcess.html#ae0ff4dca298cf634c9730305a4918e26',1,'Process']]],
  ['present_228',['present',['../structPagetableEntry.html#a59889b4f9a6ace59c4a0c52826336a13',1,'PagetableEntry']]],
  ['processes_229',['processes',['../classOS.html#a750a3c10d63414aea9bbab1f415bfd88',1,'OS']]]
];
