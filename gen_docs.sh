#! /bin/bash

doxygen DOXYGEN

make all -C docs/doxygen/latex

cp docs/doxygen/latex/refman.pdf ./docs/doxygen/doxygen_docs.pdf

make clean -C docs/doxygen/latex