#include <iostream>

#include "setup.h"
#include "NRUPageReplacer.h"
#include "FIFOPageReplacer.h"
#include "BubbleAddressGenerator.h"
#include "RandomWalkAddressGenerator.h"
#include "OS.h"

using namespace std;

int main() {
    auto os = OS();

    os.set_replacer<NRUPageReplacer>();
    //os.set_replacer<FIFOPageReplacer>();
    //os.set_address_generator(new BubbleAddressGenerator());
    os.set_address_generator(new RandomWalkAddressGenerator());

    os.startSim(700);
    return 0;
}
