//
// Created by joelr on 09.06.2022.
//

#include "setup.h"
#include "cmath"

/**
 * Holds utility functions to operate on RAM
 */
namespace RAMUtil {

    /**
     * @brief Searches a free frame in a RAM.
     *
     * Searches the std::min value of the RAM size or the #ADDRESS_ROOM.
     *
     * @param ram The ram to check for a free frame
     * @return A free frame in the ram or -1 if no free frame was found
     */
    inline int get_next_free_frame(RAM *ram) {

        for(int i=0;i<std::min(ram->size(), (unsigned long) ADDRESS_ROOM);++i) {

            //One zero byte section
            if(ram->at(i) == static_cast<std::byte>(0) && i + (PAGE_SIZE / 8) - 1 < ram->size()) {

                bool hasFree = true;

                //search for page size free frame
                for(int j=1;j<PAGE_SIZE/8;++j) {
                    //found one not free part
                    if(ram->at(i+j) != static_cast<std::byte>(0)) {
                        i += j;
                        hasFree = false;
                        break;
                    }
                }

                if(hasFree) {
                    return i;
                }
            }
        }

        return -1;
    }
}