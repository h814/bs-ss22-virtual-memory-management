//
// Created by joelr on 09.06.2022.
//

#ifndef AUFGABE3_SETUP_H
#define AUFGABE3_SETUP_H

#include <array>

#include "include/gcem.hpp"
#include "Pagetable.h"

#define ADDRESS_SIZE 8
#define OFFSET_BITS  5
#define PROCESS_COUNT  2
#define PAGE_SIZE  32
#define RAM_SIZE  128

constexpr int PAGE_INDEX_BITS = ADDRESS_SIZE - OFFSET_BITS;

constexpr int PAGES_RAM_CNT = RAM_SIZE / PAGE_SIZE;
constexpr int ADDRESS_ROOM = (int) gcem::pow(2, ADDRESS_SIZE);
constexpr int PAGES_PROCESS_CNT = (int) ADDRESS_ROOM / PAGE_SIZE;

using Pagetable =  std::array<PagetableEntry*, PAGES_PROCESS_CNT>;
using RAM = std::array<std::byte, RAM_SIZE / 8>;
using Disk = std::array<std::byte, (PAGE_SIZE / 8) * PAGES_PROCESS_CNT * PROCESS_COUNT>;

/**
 * tuple of frame start index, frame end index, process id
 */
using integritiy_map = std::array<std::tuple<int, int, int>, (RAM_SIZE / 8) / (PAGE_SIZE / 8)>;
#endif //AUFGABE3_SETUP_H
